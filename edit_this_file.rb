class HelloWorld
  def initialize(name)
    @name = name.capitalize
  end
  
  def say_hi
    puts "Hello #{@name}!"
  end

  def say_bye
    puts "Good-bye #{@name}!"
  end
end
